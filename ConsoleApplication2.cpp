#include <iostream>

const int N = 11;

int main()
{
    
    std::cout << "Enter 1 for a list of odd numbers \n"<< "Enter 2 for a list of even numbers \n";
    int Num;
    std::cin >> Num;
    
    if (Num == 1)
        for (int x = 0; x < N; ++x)
        {
            if (x % 2 == 0)
                std::cout << x << '\n';

        }
    
    if (Num == 2)
        for (int x = 0; x < N; ++x)
        {
            if (x % 2)
                std::cout << x << '\n';

        }
    else
        std::cout << "Wrong Number. Try again.";
            
    return 0;
}

